type LayoutProps = {
  children: React.ReactNode;
};

export function Layout({ children }: LayoutProps) {
  return <div className="min-h-svh font-mono">{children}</div>;
}
