type User = {
  id: string;
  lists: List[];
  username: string;
  googleId: string;
};

type List = {
  id: string;
  name: string;
  items: Item[];
};

type Item = {
  id: string;
  name: string;
  listId: string;
  svg: string | null;
  status: boolean;
  isDeleted: boolean;
};

type CreateType<T> = Omit<
  T,
  "id" | "isDeleted" | "items" | "createdAt" | "updatedAt"
>;

type ListWithUsers = List & { users: User[] };

interface ServerToClientEvents {
  add: () => (items: Item[]) => void;
  broadcastListStatus: (items: Item[]) => void;
  broadcastUsersLists: (user: ListWithUsers[]) => void;
}

interface ClientToServerEvents {
  addNewItem: (item: CreateType<Item>) => void;
  addItem: (item: Item) => void;
  removeItem: (item: Item) => void;
  editItem: (item: Item) => void;
  deleteItem: (item: Item) => void;
  addNewList: (list: CreateType<List>) => void;
  editList: (list: List) => void;
  deleteList: (list: List) => void;
  requestListItems: () => void;
}

type SimplifiedIconReponse = {
  icons: Array<{ svg: string; icon_id: number }>;
};

type IconReponse = SimplifiedIconReponse;

export type {
  User,
  Item,
  ListWithUsers,
  CreateType,
  ServerToClientEvents,
  ClientToServerEvents,
  IconReponse,
};
