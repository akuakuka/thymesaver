import {
  Sheet,
  SheetContent,
  SheetHeader,
  SheetTitle,
  SheetTrigger,
} from "@/components/ui/sheet";
import { Menu, Pencil } from "lucide-react";
import { Button } from "./components/ui/button";

import { Card } from "./components/ui/card";
import { LetterIcon } from "./components/letterIcon";
import { Badge } from "@/components/ui/badge";
import { ListWithUsers } from "@sharedTypes";
import { useState } from "react";
import { useTheme } from "./hooks/useTheme";
import { useAppStateContext } from "./hooks/useAppState";
import { ListDialog } from "./components/listDialog";
import { useSocketCtx } from "./hooks/useSocket";
import { useAuth } from "./hooks/useAuth";

export const SheetNav = () => {
  const { setTheme, theme } = useTheme();

  const { setActiveList, activeList, lists } = useAppStateContext();
  const [open, setOpen] = useState<boolean>(false);
  const [listDialogOpen, setListDialogOpen] = useState<boolean>(false);
  const [listToEdit, setListToEdit] = useState<ListWithUsers | null>(null);
  const { socket } = useSocketCtx();
  const auth = useAuth();
  const handleGoogleLogout = () => {
    socket.disconnect();
    localStorage.removeItem("JWT_TOKEN");
    auth?.logOut();
  };

  const handleListChange = (list: ListWithUsers) => {
    setActiveList(list);
    setOpen(false);
  };

  const handleSheetClosing = (open: boolean) => {
    setOpen(open);
    setListToEdit(null);
  };

  return (
    <Sheet open={open} onOpenChange={(val) => handleSheetClosing(val)}>
      <SheetTrigger className="pl-4 pt-4">
        <Menu color="#E11D48" />
      </SheetTrigger>

      <SheetContent side={"left"} className="flex flex-col h-screen font-mono">
        <SheetHeader>
          <SheetTitle>Menu</SheetTitle>
          <Button onClick={() => setTheme(theme === "dark" ? "light" : "dark")}>
            {theme}
          </Button>
        </SheetHeader>

        <div className="flex flex-col">
          {lists.map((list) => (
            <Card
              key={list.id}
              className={`${
                list.id === activeList?.id && "bg-secondary"
              } w-full h-28 flex flex-col justify-between align-middle my-2 cursor-pointer`}
              onClick={() => {
                handleListChange(list);
              }}
            >
              <div className="flex flex-row  h-1/2">
                <div className="flex flex-col p-2 gap-2">
                  <h1> {list.name}</h1>
                  <Badge className="max-w-20">{`${list.items.length} items`}</Badge>
                </div>
              </div>
              <div className="flex flex-row justify-between items-end h-1/2">
                <div className="flex flex-row justify-start p-2 gap-1">
                  {list.users.map((u) => (
                    <span
                      className="relative flex h-6 w-6 shrink-0 overflow-hidden rounded-full"
                      key={u.id}
                    >
                      <span className="flex h-full w-full items-center justify-center rounded-full bg-primary ">
                        <LetterIcon
                          letter={u.username.split("")[0]}
                          variant="small"
                        />
                      </span>
                    </span>
                  ))}
                </div>
                <Button
                  className="m-2 align-bottom"
                  onClick={(e) => {
                    e.stopPropagation();
                    setListDialogOpen(true);
                    setListToEdit(list);
                  }}
                >
                  <Pencil />
                </Button>
              </div>
            </Card>
          ))}

          <ListDialog
            isEdit={Boolean(listToEdit)}
            open={listDialogOpen}
            setOpen={setListDialogOpen}
            listToEdit={listToEdit}
            setListToEdit={setListToEdit}
          />
        </div>
        <div className="flex flex-col justify-between mt-auto">
          <Button onClick={() => handleGoogleLogout()}>Logout</Button>
        </div>
      </SheetContent>
    </Sheet>
  );
};
