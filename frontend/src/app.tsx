import { Layout } from "./layout";
import { useAuth } from "./hooks/useAuth";
import { Login } from "./pages/login";
import { SheetNav } from "./sheetnav";

import { ItemDialog } from "./components/itemDialog";
import { ItemContainer } from "./components/itemcontainer";
import SocketContainer from "./components/socketContainer";
import { LoadingSpinner } from "./components/loadingSpinner";

export default function App() {
  const auth = useAuth();

  if (auth?.isLoading) return <LoadingSpinner />;
  if (!auth?.user) return <Login />;

  return (
    <SocketContainer>
      <Layout>
        <SheetNav />
        <ItemContainer />
        <ItemDialog />
      </Layout>
    </SocketContainer>
  );
}
