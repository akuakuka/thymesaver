import { Input } from "./ui/input";
import { CreateType, Item } from "@sharedTypes";
import { Card, CardContent } from "./ui/card";
import {
  Drawer,
  DrawerContent,
  DrawerDescription,
  DrawerHeader,
  DrawerTitle,
  DrawerTrigger,
} from "./ui/drawer";
import { LetterIcon } from "./letterIcon";

import { useSocketCtx } from "@/hooks/useSocket";

import { useState } from "react";
import { BadgePlus } from "lucide-react";
import { IconDialog } from "./iconDialog";
import { useAppStateContext } from "@/hooks/useAppState";

export const ItemDialog = () => {
  const { items, activeList } = useAppStateContext();
  const { socket } = useSocketCtx();
  const [value, setValue] = useState("");
  const [open, setOpen] = useState(false);
  const [iconDialogOpen, setIconDialogOpen] = useState<boolean>(false);

  const handleAddItem = (item: CreateType<Item> | Item) => {
    // TODO: "addnew" and "add" combine
    if ("id" in item) {
      socket.emit("addItem", item);
      setOpen(false);
    } else {
      // setDialogStatus(DialogType.ICON_DRAWER);
      setIconDialogOpen(true);
    }
  };

  const handleClose = (closed: boolean) => {
    setOpen(closed);
    setValue("");
  };

  const handleIconDialogOpen = (open: boolean) => {
    setIconDialogOpen(open);
    setOpen(open);
  };
  if (!activeList?.id) return;

  return (
    <Drawer
      open={open}
      dismissible={true}
      onOpenChange={(val) => handleClose(val)}
    >
      <DrawerTrigger>
        <div className="fixed right-0 bottom-0 p-4">
          <BadgePlus
            // onClick={() => setDialogStatus(DialogType.ITEM_DRAWER)}
            size={32}
            color="#E11D48"
          />
        </div>
      </DrawerTrigger>
      <DrawerContent className="font-mono">
        <DrawerHeader>
          <DrawerTitle>Add Item</DrawerTitle>
          <DrawerDescription>Add new item and pick a icon</DrawerDescription>
        </DrawerHeader>

        <div className="flex flex-row flex-wrap p-2 m-2 gap-2">
          {[
            { name: value, svg: "", status: false, listId: activeList.id },
            ...items,
          ]
            .filter((item) =>
              item.name.toLocaleLowerCase().includes(value.toLocaleLowerCase())
            )
            .filter((i) => i.name.length > 0)
            .filter((i) => !i.status)
            .splice(0, 12)
            .map((item, i) => (
              <Card
                className={`w-24 ${item.name === value && "bg-secondary"}`}
                key={i}
                onClick={() =>
                  handleAddItem({ ...item, listId: activeList.id })
                }
              >
                <CardContent className="flex flex-col">
                  {item.svg ? (
                    <img
                      src={`data:image/svg+xml;utf8,${encodeURIComponent(
                        item.svg
                      )}`}
                      className="filter min-h-8"
                    />
                  ) : (
                    <LetterIcon letter={item.name.split("")[0]} />
                  )}

                  <div> {item.name}</div>
                </CardContent>
              </Card>
            ))}
        </div>

        <div className="p-4">
          <Input
            placeholder="name"
            value={value}
            onChange={(e) => setValue(e.target.value)}
          />
        </div>
      </DrawerContent>
      <IconDialog
        open={iconDialogOpen}
        setOpen={handleIconDialogOpen}
        initialItemName={value}
      />
    </Drawer>
  );
};
