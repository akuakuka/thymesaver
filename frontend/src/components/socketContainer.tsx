import { useAppStateContext } from "@/hooks/useAppState";
import { useAuth } from "@/hooks/useAuth";
import useLocalStorage from "@/hooks/useLocalStorage";
import { useSocketCtx } from "@/hooks/useSocket";
import { Item, ListWithUsers } from "@/types/types";
import { useEffect } from "react";

type ChildrenProps = {
  children: string | JSX.Element | JSX.Element[];
};

export default function SocketContainer({ children }: ChildrenProps) {
  const { socket } = useSocketCtx();
  const { setItems, setLists } = useAppStateContext();
  const auth = useAuth();
  const [JWT_TOKEN] = useLocalStorage("JWT_TOKEN", "");

  useEffect(() => {
    const onConnect = () => {
      console.log("ONCONNECT");
      socket.emit("requestListItems");
    };

    socket.on("connect_error", (err) => {
      console.log("connect_error");
      console.error(err);
    });

    socket.on("connect", onConnect);

    const onbroadcastListStatus = (items: Item[]) => {
      setItems(items);
    };

    const onBroadcastUsersLists = (lists: ListWithUsers[]) => {
      setLists(lists);
    };

    socket.on("broadcastListStatus", onbroadcastListStatus);
    socket.on("broadcastUsersLists", onBroadcastUsersLists);

    return () => {
      socket.off("connect", onConnect);
      socket.off("broadcastListStatus", onbroadcastListStatus);
    };
  }, [setItems, setLists, socket, auth, JWT_TOKEN]);

  return <>{children} </>;
}
