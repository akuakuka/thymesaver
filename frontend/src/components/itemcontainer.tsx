import { Item } from "@sharedTypes";
import { useEffect, useState } from "react";

import { useSocketCtx } from "@/hooks/useSocket";
import { EmptyIllustration } from "./emptylistillustration";
import { ItemDetailDrawer } from "./itemDetailDrawer";
import { useAppStateContext } from "@/hooks/useAppState";
import { ItemCard } from "./itemcard";

export const ItemContainer = () => {
  const [filteredItems, setFilteredItems] = useState<Item[]>([]);
  const [itemToEdit, setItemToEdit] = useState<Item | null>(null);
  const [itemDetailOpen, setItemDetailOpen] = useState<boolean>(false);

  const { socket } = useSocketCtx();
  const { items, activeList } = useAppStateContext();

  useEffect(() => {
    if (activeList?.id) {
      setFilteredItems(items.filter((i) => i.status));
    }
  }, [items, activeList]);

  useEffect(() => {
    if (itemToEdit) {
      setItemDetailOpen(true);
    } else {
      setItemDetailOpen(false);
    }
  }, [itemToEdit]);

  const handleRemoveItem = (item: Item) => {
    socket.emit("removeItem", item);
  };

  const handleItemDetailClosing = (open: boolean) => {
    setItemDetailOpen(open);
    setItemToEdit(null);
  };

  const activeItemsCount = items.filter((i) => i.status).length;

  if (!activeList)
    return <div className="flex flex-col p-4 mx-8 ">No list selected !</div>;
  return (
    <div className="flex flex-col p-4 mx-8 ">
      <h4 className="mb-8">
        {activeList?.name} - ({activeItemsCount} / {items.length} items)
      </h4>
      <div className="flex flex-row flex-wrap gap-2">
        {filteredItems.length === 0 && (
          <>
            <h1>Empty List ! </h1>
            <EmptyIllustration />
          </>
        )}
        {filteredItems.map((i) => (
          <ItemCard
            key={i.id}
            item={i}
            handleRemoveItem={() => handleRemoveItem(i)}
            setItemToEdit={setItemToEdit}
          />
        ))}
      </div>
      <ItemDetailDrawer
        item={itemToEdit}
        open={itemDetailOpen}
        setOpen={handleItemDetailClosing}
      />
    </div>
  );
};
