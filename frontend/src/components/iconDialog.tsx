import {
  Dialog,
  DialogContent,
  DialogHeader,
  DialogTitle,
} from "@/components/ui/dialog";
import { Input } from "./ui/input";
import { CreateType, Item } from "@sharedTypes";
import { LoaderIcon } from "lucide-react";
import { LetterIcon } from "./letterIcon";
import { useSocketCtx } from "@/hooks/useSocket";
import { useIcons } from "@/hooks/useQuery";
import { useEffect, useState } from "react";
import { useDebounce } from "@/hooks/useDebounce";

import { DialogTrigger } from "@radix-ui/react-dialog";
import { useAppStateContext } from "@/hooks/useAppState";

type IconDialogProps = {
  open: boolean;
  setOpen: (opeen: boolean) => void;
  initialItemName: string;
};

export const IconDialog = ({
  open,
  setOpen,
  initialItemName,
}: IconDialogProps) => {
  const [iconSearchTerm, setIconSearchTerm] = useState<string>(initialItemName);
  const { socket } = useSocketCtx();
  const { activeList } = useAppStateContext();
  const debouncedValue = useDebounce(iconSearchTerm);
  const { status, data, error, refetch, isFetching } = useIcons(debouncedValue);

  useEffect(() => {
    if (open) {
      setIconSearchTerm(initialItemName);
    }
  }, [initialItemName, open]);

  useEffect(() => {
    if (debouncedValue?.length < 3 || !open) return;
    refetch();
  }, [debouncedValue, open, refetch]);

  const handleIconClick = (item: Item | CreateType<Item>) => {
    if ("id" in item) {
      socket.emit("addItem", item);
    } else {
      socket.emit("addNewItem", item);
    }
    handleClose();
  };

  const handleClose = () => {
    setOpen(false);
  };

  if (!activeList) return;

  return (
    <Dialog open={open} onOpenChange={() => handleClose()}>
      <DialogTrigger />
      <DialogContent className="font-mono">
        <DialogHeader>
          <DialogTitle className="pb-7">
            Choose icon for {initialItemName}
          </DialogTitle>
          <div>
            <div className="flex flex-col">
              <Input
                value={iconSearchTerm}
                onChange={(e) => setIconSearchTerm(e.target.value)}
                className="mb-5"
                placeholder={initialItemName}
              />
              <div>
                {isFetching ? (
                  <LoaderIcon className="animate-spin" />
                ) : status === "error" ? (
                  <span>Error: {error.message}</span>
                ) : (
                  <div className="container flex flex-row flex-wrap gap-2 overflow-scroll max-h-72">
                    {data &&
                      [
                        {
                          name: initialItemName,
                          svg: null,
                          status: true,
                          listId: activeList.id,
                          icon_id: "dummy",
                        },
                        ...data.icons,
                      ].map((i) => (
                        <div
                          key={i.icon_id}
                          className="bg-secondary p-2 min-w-20 max-w-20 min-h-20 max-h-20"
                          onClick={() =>
                            handleIconClick({
                              name: initialItemName,
                              svg: i.svg,
                              status: true,
                              listId: activeList.id,
                            })
                          }
                        >
                          {i.svg ? (
                            <img
                              src={`data:image/svg+xml;utf8,${encodeURIComponent(
                                i.svg
                              )}`}
                              className="filter max-h-12"
                            />
                          ) : (
                            <LetterIcon letter={initialItemName.split("")[0]} />
                          )}
                        </div>
                      ))}
                  </div>
                )}
              </div>
            </div>
          </div>
        </DialogHeader>
      </DialogContent>
    </Dialog>
  );
};
