import { Button } from "@/components/ui/button";
import {
  Dialog,
  DialogContent,
  DialogDescription,
  DialogFooter,
  DialogHeader,
  DialogTitle,
  DialogTrigger,
} from "@/components/ui/dialog";
import { Switch } from "@/components/ui/switch";
import { Input } from "@/components/ui/input";
import { Label } from "@/components/ui/label";
import { useSocketCtx } from "@/hooks/useSocket";
import { ListWithUsers } from "@sharedTypes";
import { useEffect, useState } from "react";
import useLocalStorage from "@/hooks/useLocalStorage";

type ListDialogProps = {
  isEdit: boolean;
  open: boolean;
  setOpen: (open: boolean) => void;
  listToEdit: ListWithUsers | null;
  setListToEdit: (list: ListWithUsers | null) => void;
};
export const ListDialog = ({
  isEdit,
  open,
  setOpen,
  listToEdit = null,
  setListToEdit,
}: ListDialogProps) => {
  const [newName, setnewName] = useState<string>("");
  const { socket } = useSocketCtx();
  const [defaultListId, setDefaultListId] = useLocalStorage(
    "defaultListId",
    ""
  );

  useEffect(() => {
    if (isEdit && listToEdit) {
      setnewName(listToEdit.name);
    }
  }, [listToEdit, isEdit]);

  const handleSave = () => {
    if (isEdit) {
      if (!listToEdit) return;
      socket.emit("editList", { ...listToEdit, name: newName });
    } else {
      socket.emit("addNewList", { name: newName });
    }

    setOpen(false);
  };

  const handleDelete = () => {
    // deleteRefetch();
    if (!listToEdit) return;
    socket.emit("deleteList", listToEdit);
    setOpen(false);
  };

  const isSaveDisabled = (): boolean => {
    if (isEdit) {
      return newName === listToEdit?.name && Boolean(newName.length);
    }
    return Boolean(!newName.length);
  };
  const handleDefaultListSwitch = (state: boolean) => {
    if (state && listToEdit) {
      setDefaultListId(listToEdit.id);
    }
  };

  return (
    <Dialog
      open={open}
      onOpenChange={(val) => {
        setOpen(val);
        setListToEdit(null);
      }}
    >
      <DialogTrigger asChild onClick={() => setOpen(true)}>
        <Button>{isEdit ? <>Edit List</> : <>Create new</>}</Button>
      </DialogTrigger>
      <DialogContent className="sm:max-w-[425px]">
        <DialogHeader>
          <DialogTitle>{isEdit ? <>Edit List</> : <>Create new</>}</DialogTitle>
          <DialogDescription>
            {isEdit ? (
              <div className="flex flex-row justify-between">
                Edit List
                <div className="flex items-center space-x-2 pr-6">
                  <Switch
                    id="defaultlist"
                    checked={listToEdit?.id === defaultListId}
                    onCheckedChange={(e) => handleDefaultListSwitch(e)}
                  />
                  <Label htmlFor="airplane-mode">Default list</Label>
                </div>
              </div>
            ) : (
              <> Create new list. You can only create 4 lists</>
            )}
          </DialogDescription>
        </DialogHeader>
        <div className="grid gap-4 py-4">
          <div className="grid grid-cols-4 items-center gap-4">
            <Label htmlFor="name" className="text-right">
              Name
            </Label>
            <Input
              id="name"
              className="col-span-3"
              value={newName}
              onChange={(e) => setnewName(e.target.value)}
            />
          </div>
        </div>
        <DialogFooter>
          {isEdit && (
            <Button
              variant={"destructive"}
              type="submit"
              onClick={() => handleDelete()}
            >
              Delete
            </Button>
          )}

          <Button
            type="submit"
            onClick={() => handleSave()}
            disabled={isSaveDisabled()}
          >
            Save
          </Button>
        </DialogFooter>
      </DialogContent>
    </Dialog>
  );
};
