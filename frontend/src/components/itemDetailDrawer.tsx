import { useEffect, useState } from "react";
import { LetterIcon } from "./letterIcon";
import {
  Drawer,
  DrawerContent,
  DrawerDescription,
  DrawerHeader,
  DrawerTitle,
} from "./ui/drawer";
import { Input } from "./ui/input";
import { Button } from "./ui/button";

import { useSocketCtx } from "@/hooks/useSocket";
import { Item } from "@sharedTypes";

type ItemDetailDrawerProps = {
  item: Item | null;
  open: boolean;
  setOpen: (open: boolean) => void;
};

export const ItemDetailDrawer = ({
  item,
  open,
  setOpen,
}: ItemDetailDrawerProps) => {
  const [newName, setNewName] = useState<string>("");
  const [newSvg, setNewSvg] = useState<string | null>("");

  const { socket } = useSocketCtx();

  useEffect(() => {
    if (item) {
      setNewName(item.name);
      setNewSvg(item.svg);
    }
  }, [item]);

  if (!item) return null;

  const handleSave = async () => {
    socket.emit("editItem", { ...item, name: newName });
    setOpen(false);
    socket.emit("requestListItems");
  };

  const handleDelete = async () => {
    socket.emit("deleteItem", item);

    setOpen(false);
    socket.emit("requestListItems");
  };

  const isSaveDisabled = Boolean(item.name === newName && item.svg === newSvg);

  if (!item) return;

  return (
    <Drawer open={open} onClose={() => setOpen(false)}>
      <DrawerContent>
        <DrawerHeader>
          <DrawerTitle>Item details</DrawerTitle>
          <DrawerDescription>Edit item</DrawerDescription>
        </DrawerHeader>
        <div className="flex flex-row justify-center">
          <div className="flex flex-col justify-center">
            {item?.svg ? (
              <img
                src={`data:image/svg+xml;utf8,${encodeURIComponent(item.svg)}`}
                className="filter min-h-48"
              />
            ) : (
              <LetterIcon letter={item.name.split("")[0]} />
            )}

            <Input
              placeholder="name"
              value={newName}
              onChange={(e) => setNewName(e.target.value)}
            />
            <div className="flex flex-row justify-between">
              <Button onClick={() => handleDelete()}>Delete</Button>
              <Button onClick={() => handleSave()} disabled={isSaveDisabled}>
                Save
              </Button>
            </div>
          </div>
        </div>
      </DrawerContent>
    </Drawer>
  );
};
