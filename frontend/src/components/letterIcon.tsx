type LetterIconProps = {
  letter: string;
  variant?: "small" | "medium" | "large";
};
export const LetterIcon = ({ letter, variant = "large" }: LetterIconProps) => {
  if (variant === "large") {
    return <h1 className={`text-6xl font-Silkscreen`}>{letter}</h1>;
  }
  if (variant === "medium") {
    return <h1 className={`text-2xl font-Silkscreen`}>{letter}</h1>;
  }
  if (variant === "small") {
    return <h1 className={`text-xl font-Silkscreen`}>{letter}</h1>;
  }
  return null;
};
