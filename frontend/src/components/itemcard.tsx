import type { Item } from "@sharedTypes";
import { Card, CardContent } from "./ui/card";
import { LetterIcon } from "./letterIcon";
import { useCallback, useState } from "react";
import { LongPressEventType, useLongPress } from "use-long-press";
import { PresenceAnimation } from "./animations/presence";

type ItemProps = {
  item: Item;
  handleRemoveItem: (item: Item) => void;
  setItemToEdit: (item: Item) => void;
};

export const ItemCard = ({
  item,
  handleRemoveItem,
  setItemToEdit,
}: ItemProps) => {
  const [enabled] = useState(true);

  const callback = useCallback(() => {
    setItemToEdit(item);
  }, [item, setItemToEdit]);

  const bind = useLongPress(enabled ? callback : null, {
    // onStart: () => console.log("Press started"),
    // onFinish: () => console.log("Long press finished"),
    // onCancel: () => console.log("Press cancelled"),
    // onMove: () => console.log("Detected mouse or touch movement"),
    filterEvents: () => true, // All events can potentially trigger long press (same as 'undefined')
    threshold: 1500, // In milliseconds
    captureEvent: true, // Event won't get cleared after React finish processing it
    cancelOnMovement: 25, // Square side size (in pixels) inside which movement won't cancel long press
    cancelOutsideElement: true, // Cancel long press when moved mouse / pointer outside element while pressing
    detect: LongPressEventType.Pointer, // Default option
  });

  return (
    <PresenceAnimation>
      <button {...bind()}>
        <Card
          className="flex flex-col min-w-28 min-h-24 max-w-28 max-h-36 justify-between"
          key={item.id}
          onClick={() => handleRemoveItem(item)}
        >
          <CardContent>
            {item.svg ? (
              <img
                src={`data:image/svg+xml;utf8,${encodeURIComponent(item.svg)}`}
                style={{ userSelect: "none" }}
              />
            ) : (
              <LetterIcon letter={item.name.split("")[0]} />
            )}
          </CardContent>

          <span className="font-mono text-ellipsis whitespace-nowrap overflow-hidden">
            {item.name}
          </span>
        </Card>
      </button>
    </PresenceAnimation>
  );
};
