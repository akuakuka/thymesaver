import { motion, AnimatePresence } from "framer-motion";

type PresenceProps = {
  children: React.ReactNode;
};

export const PresenceAnimation = ({ children }: PresenceProps) => (
  <AnimatePresence>
    <motion.div
      initial={{ opacity: 0 }}
      animate={{ opacity: 1 }}
      exit={{ opacity: 0 }}
      whileHover={{ scale: 1.2 }}
      whileTap={{ scale: 0.4 }}
    >
      {children}
    </motion.div>
  </AnimatePresence>
);
