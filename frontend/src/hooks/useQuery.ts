import { IconReponse, User } from "@sharedTypes";
import { useQuery } from "@tanstack/react-query";
import axios from "axios";

const API = axios.create({
  baseURL: import.meta.env.VITE_BACKEND_URL,
});

API.interceptors.request.use((config) => {
  const { url, baseURL, method } = config;
  console.info(`${method?.toUpperCase()} -> ${baseURL}${url}`);
  const token = JSON.parse(localStorage.getItem("JWT_TOKEN") || "");
  config.headers["x-auth-token"] = token;
  return config;
});

export function useGetUser(token: string) {
  return useQuery({
    queryKey: ["user"],
    enabled: false,
    retry: false,
    queryFn: async () => {
      try {
        console.log(token);
        const { data } = await API.get<User>(`/users/me`);
        console.log(data);
        return data;
      } catch (e) {
        localStorage.removeItem("JWT_TOKEN");
        console.log("useGetUser error ! ");
        // eslint-disable-next-line @typescript-eslint/ban-ts-comment
        // @ts-expect-error
        console.log(e?.message);
        return Promise.reject(e);
      }
    },
  });
}

export function useIcons(searchTerm: string) {
  return useQuery({
    queryKey: ["icons"],
    enabled: false,
    queryFn: async () => {
      const { data } = await API.get<IconReponse>(`/icons/search`, {
        params: {
          searchTerm,
        },
      });
      return data;
    },
  });
}
