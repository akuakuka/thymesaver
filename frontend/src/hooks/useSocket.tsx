import { useContext } from "react";
import { ClientToServerEvents, ServerToClientEvents } from "@sharedTypes";
import { createContext } from "react";
import { Socket } from "socket.io-client";

import { ReactNode } from "react";
import { io } from "socket.io-client";
import useLocalStorage from "./useLocalStorage";

type SocketType = Socket<ServerToClientEvents, ClientToServerEvents>;

interface SocketCtxState {
  socket: SocketType;
}

const SocketCtx = createContext<SocketCtxState>({} as SocketCtxState);

const URL = import.meta.env.VITE_BACKEND_URL;

export const SocketCtxProvider = (props: { children?: ReactNode }) => {
  const [JWT_TOKEN] = useLocalStorage("JWT_TOKEN", "");
  const socket = io(URL, {
    autoConnect: true,
    retries: 10,
    extraHeaders: {
      "x-auth-token": `${JWT_TOKEN}`,
    },
  });
  return (
    <SocketCtx.Provider value={{ socket: socket }}>
      {props.children}
    </SocketCtx.Provider>
  );
};

export const useSocketCtx = () => useContext(SocketCtx);
