import { User } from "@sharedTypes";
import { createContext, useEffect, useState } from "react";
import { useGetUser } from "./useQuery";

type ContextType = {
  user: User | null;
  isLoading: boolean;
  logOut: () => void;
};
const AuthContext = createContext<ContextType | null>(null);

type ChildrenProps = {
  children: string | JSX.Element | JSX.Element[];
};

import { useContext } from "react";
import useLocalStorage from "./useLocalStorage";

export const AuthProvider = ({ children }: ChildrenProps) => {
  const [JWT_TOKEN, setJWT_TOKEN] = useLocalStorage("JWT_TOKEN", "");
  const [user, setUser] = useState<User | null>(null);
  const { data, isFetching, isLoading, refetch } = useGetUser(JWT_TOKEN);

  useEffect(() => {
    const params = new URLSearchParams(window.location.search);
    const token = params.get("token");

    if (token) {
      setJWT_TOKEN(token);
      window.location.href = "/";
    }
  }, [JWT_TOKEN, setJWT_TOKEN]);

  useEffect(() => {
    (async () => {
      if (!JWT_TOKEN) return;
      try {
        await refetch();
      } catch (e) {
        console.error(e);
      }
    })();
  }, [JWT_TOKEN, refetch]);

  useEffect(() => {
    if (!data) return;
    setUser(data);
  }, [data]);

  return (
    <AuthContext.Provider
      value={{
        user: user,
        isLoading: Boolean(isFetching || isLoading),
        logOut: () => setUser(null),
      }}
    >
      {children}
    </AuthContext.Provider>
  );
};

export const useAuth = () => {
  return useContext(AuthContext);
};
