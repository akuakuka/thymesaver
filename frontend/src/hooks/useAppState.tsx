import { useContext } from "react";
import { Item, ListWithUsers } from "@sharedTypes";
import { PropsWithChildren, createContext, useEffect, useState } from "react";
import useLocalStorage from "./useLocalStorage";

type AppStateContextType = {
  items: Item[];
  lists: ListWithUsers[];
  setLists: (lists: ListWithUsers[]) => void;
  setItems: (items: Item[]) => void;
  newItemName: string;
  setNewItemName: (name: string) => void;
  activeList: ListWithUsers | null;
  setActiveList: (list: ListWithUsers) => void;
};

const AppStateContext = createContext<AppStateContextType | undefined>(
  undefined
);

export const useAppStateContext = () => {
  const context = useContext(AppStateContext);

  if (!context) {
    throw new Error("useAppStateContext must be used inside the ItemProvider");
  }

  return context;
};

export const AppStateProvider = ({ children }: PropsWithChildren<object>) => {
  const [items, setItems] = useState<AppStateContextType["items"]>([]);
  const [lists, setLists] = useState<AppStateContextType["lists"]>([]);
  const [activeListItems, setActiveListItems] = useState<
    AppStateContextType["items"]
  >([]);

  const [activeList, setActiveList] =
    useState<AppStateContextType["activeList"]>(null);
  const [newItemName, setNewItemName] =
    useState<AppStateContextType["newItemName"]>("");

  const [defaultListId] = useLocalStorage("defaultListId", "");

  useEffect(() => {
    const filteredItems = items.filter((i) => i.listId === activeList?.id);
    setActiveListItems(filteredItems);
  }, [activeList, items]);

  useEffect(() => {
    if (activeList === null && lists.length) {
      const defaultList = lists.find((list) => list.id === defaultListId);
      if (defaultList) {
        setActiveList(defaultList);
      } else {
        setActiveList(lists[0]);
      }
    }
  }, [activeList, defaultListId, lists]);

  return (
    <AppStateContext.Provider
      value={{
        items: activeListItems,
        setItems,
        newItemName,
        setNewItemName,
        activeList,
        setActiveList,
        lists,
        setLists,
      }}
    >
      {children}
    </AppStateContext.Provider>
  );
};
