import { Button } from "@/components/ui/button";

export function Login() {
  const handleGoogleLogin = () => {
    window.location.href = `${import.meta.env.VITE_BACKEND_URL}/auth/google`;
  };

  return (
    <div className="flex flex-col min-h-svh justify-center align-middle">
      <div className="flex flex-row justify-center">
        <div className="">
          <h1 className="text-2xl m-6 ">Thymesaver</h1>
          <Button
            size={"lg"}
            onClick={() => handleGoogleLogin()}
            className="max-w-40"
          >
            Login with Google
          </Button>
          <div>AUTH MIDDLEWARE DEBUG</div>
          <div>VITE_BACKEND_URL</div>
          <div>{JSON.stringify(import.meta.env.VITE_BACKEND_URL)}</div>
          <div>BASE_URL</div>
          <div>{JSON.stringify(import.meta.env.BASE_URL)}</div>
        </div>
      </div>
    </div>
  );
}
