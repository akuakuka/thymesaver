import React from "react";
import ReactDOM from "react-dom/client";
import "./globals.css";

import { Toaster } from "./components/ui/sonner";

import { QueryClient, QueryClientProvider } from "@tanstack/react-query";
import { AuthProvider } from "./hooks/useAuth";
import { SocketCtxProvider } from "./hooks/useSocket";
import { ThemeProvider } from "./hooks/useTheme";
import { AppStateProvider } from "./hooks/useAppState";
import App from "./app";

const queryClient = new QueryClient();

ReactDOM.createRoot(document.getElementById("root")!).render(
  <React.StrictMode>
    <QueryClientProvider client={queryClient}>
      <AuthProvider>
        <ThemeProvider defaultTheme="dark" storageKey="vite-ui-theme">
          <SocketCtxProvider>
            <AppStateProvider>
              <App />
            </AppStateProvider>
            <Toaster />
          </SocketCtxProvider>
        </ThemeProvider>
      </AuthProvider>
    </QueryClientProvider>
  </React.StrictMode>
);
