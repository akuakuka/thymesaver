import express, {
  type NextFunction,
  type Request,
  type Response,
  json,
} from "express";
import { env } from "./env/server";

import cors from "cors";
import passport from "passport";
import { prisma } from "./db/db";
import { authRouter } from "./routers/authRouter";
import { userRouter } from "./routers/userRouter";
import { iconRouter } from "./routers/iconRouter";
import http from "http";
import { Server } from "socket.io";
import {
  type User,
  type ClientToServerEvents,
  type InterServerEvents,
  type ServerToClientEvents,
  type SocketData,
} from "./types/shared";
import {
  changeItemStatus,
  createNewItem,
  deleteITem,
  editItem,
  getAllItems,
} from "./controllers/itemController";
import { itemRouter } from "./routers/itemRouter";
import {
  createNewList,
  deleteList,
  editList,
  getUsersList,
} from "./controllers/listController";

import winston from "winston";
import { googleStrategy } from "./passport/google";
import requireJwtAuth from "./middleware/jwt";
import { jwtStrategy } from "./passport/jwtStrategy";

const app = express();
const server = http.createServer(app);

const port = env.PORT;

export const logger = winston.createLogger({
  level: "info",
  format: winston.format.json(),
  transports: [new winston.transports.Console()],
});

app.use(
  cors({
    origin: "*",
    credentials: false,
    methods: ["GET,HEAD,PUT,PATCH,POST,DELETE"],
  }),
);
app.use(json());
app.use(express.urlencoded({ extended: true }));

app.use(passport.initialize());
passport.use(jwtStrategy);
passport.use(googleStrategy);

passport.serializeUser((user, done) => {
  console.log("serializeUser");
  done(null, user);
});

passport.deserializeUser(async (user: any, done) => {
  console.log("deserializeUser");
  const usr = await prisma.user.findFirst({
    where: {
      id: user.id,
    },
  });
  done(null, usr);
});

// eslint-disable-next-line @typescript-eslint/no-unsafe-argument
app.use("/users", requireJwtAuth, userRouter);
// eslint-disable-next-line @typescript-eslint/no-unsafe-argument
app.use("/icons", requireJwtAuth, iconRouter);
// eslint-disable-next-line @typescript-eslint/no-unsafe-argument
app.use("/items", requireJwtAuth, itemRouter);
app.use("/auth", authRouter);

const io = new Server<
  ClientToServerEvents,
  ServerToClientEvents,
  InterServerEvents,
  SocketData
>(server, {
  cors: {
    origin: "*",
    methods: ["GET,HEAD,PUT,PATCH,POST,DELETE"],
  },
  allowEIO3: true,
  transports: ["polling", "websocket"],
  allowUpgrades: true,
});
io.listen(server);

io.engine.use(passport.initialize());

io.engine.use((req: Request, res: Response, next: NextFunction) => {
  // eslint-disable-next-line @typescript-eslint/ban-ts-comment
  // @ts-expect-error
  const isHandshake = req._query.sid === undefined;
  if (isHandshake) {
    passport.authenticate("jwt", { session: false })(req, res, next);
  } else {
    next();
  }
});

io.on("connection", async socket => {
  // eslint-disable-next-line @typescript-eslint/ban-ts-comment
  // @ts-expect-error
  const user = socket.request.user as unknown as User;

  console.log("SOCKER USER");
  console.log(user);
  if (!user) return;

  const lists = await getUsersList(user);
  io.sockets.to(socket.id).emit("broadcastUsersLists", lists);

  socket.on("requestListItems", async () => {
    const items = await getAllItems();
    io.emit("broadcastListStatus", items);
  });

  socket.on("addItem", async item => {
    await changeItemStatus(item, true, user);
    const items = await getAllItems();
    io.emit("broadcastListStatus", items);
  });

  socket.on("addNewItem", async item => {
    await createNewItem(item, user);
    const items = await getAllItems();
    io.emit("broadcastListStatus", items);
  });

  socket.on("removeItem", async item => {
    await changeItemStatus(item, false, user);
    const items = await getAllItems();
    io.emit("broadcastListStatus", items);
  });

  socket.on("editItem", async item => {
    await editItem(item, user);
    const items = await getAllItems();
    io.emit("broadcastListStatus", items);
  });

  socket.on("deleteItem", async item => {
    await deleteITem(item, user);
    const items = await getAllItems();
    io.emit("broadcastListStatus", items);
  });

  socket.on("addNewList", async list => {
    await createNewList(list, user);
    const lists = await getUsersList(user);
    io.emit("broadcastUsersLists", lists);
  });

  socket.on("editList", async list => {
    await editList(list, user);
    const lists = await getUsersList(user);
    io.emit("broadcastUsersLists", lists);
    //  io.emit("broadcastListStatus", items);
  });
  socket.on("deleteList", async list => {
    await deleteList(list, user);
    const lists = await getUsersList(user);
    io.emit("broadcastUsersLists", lists);
  });
});

app.use((err: any, req: Request, res: Response, next: NextFunction) => {
  logger.error(err.message || "Something went wrong!");
  res.status(500).json({
    error: JSON.stringify(err) || "Something went wrong!",
  });
});
app.get("/", (req, res) => {
  res.json({ message: "root route debuhg" });
});
server.listen(port, () => {
  console.info(`Server is running on http://localhost:${port}`);
});
