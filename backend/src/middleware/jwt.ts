import passport from "passport";
import jwt from "jsonwebtoken";
import type { User } from "@prisma/client";
import { env } from "../env/server";

export const generateJWT = (user: User): string => {
  const token = jwt.sign(
    {
      expiresIn: "48h",
      id: user.id,
    },
    env.SESSION_KEY,
  );
  return token;
};

const requireJwtAuth = passport.authenticate("jwt", { session: false });

export default requireJwtAuth;

// function(err, user, failuresOrInfo, status) {

//     // failuresOrInfo has the cause.

//       if (err) { return next(err) }
//      if (!user) { return res.redirect('/signin') }
//      res.redirect('/account');
//     })(req, res, next);
//   });
