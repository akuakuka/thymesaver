import { type NextFunction, type Response, type Request } from "express";

export const asyncMiddleware =
  (fn: (req: Request, res: Response, next: NextFunction) => void) =>
  async (req: Request, res: Response, next: NextFunction) => {
    // eslint-disable-next-line @typescript-eslint/no-confusing-void-expression
    await Promise.resolve(fn(req, res, next)).catch(next);
  };
