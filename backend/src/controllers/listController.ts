import { type List, type User } from "@prisma/client";
import { type ListWithUsers, type CreateType } from "../types/shared";
import { prisma } from "../db/db";
import {
  ListActionLogActionTypeEnum,
  createListActionLog,
} from "./actionLogController";

export const createNewList = async (
  list: CreateType<List>,
  user: User,
): Promise<List> => {
  const newList = await prisma.list.create({
    data: {
      name: list.name,
      users: {
        connect: [{ id: user.id }],
      },
    },
  });

  await createListActionLog({
    action: ListActionLogActionTypeEnum.LIST_CREATE,
    listId: newList.id,
    userId: user.id,
  });

  return newList;
};

export const editList = async (list: List, user: User): Promise<List> => {
  const updatedList = await prisma.list.update({
    where: { id: list.id },
    data: { name: list.name },
  });

  await createListActionLog({
    action: ListActionLogActionTypeEnum.LIST_UPDATE,
    listId: updatedList.id,
    userId: user.id,
  });

  return updatedList;
};

export const deleteList = async (list: List, user: User): Promise<List> => {
  const deletedList = await prisma.list.delete({
    where: { id: list.id },
  });

  await createListActionLog({
    action: ListActionLogActionTypeEnum.LIST_DELETE,
    listId: list.id,
    userId: user.id,
  });

  return deletedList;
};

export const getUsersList = async (user: User): Promise<ListWithUsers[]> => {
  const userWithLists = await prisma.user.findFirstOrThrow({
    where: { id: user.id },
    include: {
      lists: { include: { users: true, items: true, actionlog: true } },
    },
  });

  return userWithLists.lists;
};
