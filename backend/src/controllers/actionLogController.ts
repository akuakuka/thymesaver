import { prisma } from "../db/db";

export enum ItemActionLogActionTypeEnum {
  CREATE = "CREATE",
  UPDATE = "UPDATE",
  DELETE = "DELETE",
  ITEM_REMOVE = "ITEM_REMOVE",
  ITEM_ADD = "ITEM_ADD",
}

export enum ListActionLogActionTypeEnum {
  LIST_CREATE = "LIST_CREATE",
  LIST_UPDATE = "LIST_UPDATE",
  LIST_DELETE = "LIST_DELETE",
}

export const createItemActionLog = async ({
  action,
  itemId,
  userId,
}: {
  action: ItemActionLogActionTypeEnum;
  itemId: string;
  userId: string;
}): Promise<void> => {
  await prisma.actionLog.create({
    data: {
      action,
      userId,
      itemId,
    },
  });
};

export const createListActionLog = async ({
  action,
  listId,
  userId,
}: {
  action: ListActionLogActionTypeEnum;
  listId: string;
  userId: string;
}): Promise<void> => {
  await prisma.actionLog.create({
    data: {
      action,
      userId,
      listId,
    },
  });
};
