import { prisma } from "../db/db";
import {
  // type ActionLogActionType,
  type CreateType,
  type Item,
  type User,
} from "../types/shared";
import {
  ItemActionLogActionTypeEnum,
  createItemActionLog,
} from "./actionLogController";

export const changeItemStatus = async (
  item: Item,
  status: boolean,
  user: User,
): Promise<void> => {
  try {
    await prisma.item.update({
      where: { id: item.id },
      data: { status },
    });

    const action = status
      ? ItemActionLogActionTypeEnum.ITEM_ADD
      : ItemActionLogActionTypeEnum.ITEM_REMOVE;

    await createItemActionLog({
      action,
      itemId: item.id,
      userId: user.id,
    });
  } catch (e) {
    console.error("Error changing item status");
    await Promise.reject(e);
  }
};

export const getAllItems = async (): Promise<Item[]> => {
  try {
    const items = await prisma.item.findMany({ where: { isDeleted: false } });
    return items;
  } catch (e) {
    return await Promise.reject(e);
  }
};

export const createNewItem = async (
  item: CreateType<Item>,
  user: User,
): Promise<Item> => {
  try {
    const newItem = await prisma.item.create({ data: { ...item } });

    await createItemActionLog({
      action: ItemActionLogActionTypeEnum.CREATE,
      itemId: newItem.id,
      userId: user.id,
    });

    return newItem;
  } catch (e) {
    console.error("Error creating new item");
    return await Promise.reject(e);
  }
};

export const deleteITem = async (item: Item, user: User): Promise<Item> => {
  try {
    const deleted = await prisma.item.update({
      where: { id: item.id },
      data: { isDeleted: true },
    });

    await createItemActionLog({
      action: ItemActionLogActionTypeEnum.DELETE,
      itemId: deleted.id,
      userId: user.id,
    });

    return deleted;
  } catch (e) {
    return await Promise.reject(e);
  }
};

export const editItem = async (item: Item, user: User): Promise<Item> => {
  try {
    const edited = await prisma.item.update({
      where: { id: item.id },
      data: { ...item },
    });
    await createItemActionLog({
      action: ItemActionLogActionTypeEnum.UPDATE,
      itemId: edited.id,
      userId: user.id,
    });
    return edited;
  } catch (e) {
    return await Promise.reject(e);
  }
};
