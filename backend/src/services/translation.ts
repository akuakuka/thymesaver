import axios from "axios";
import { env } from "../env/server";

const TRANSLATION_API = axios.create({
  baseURL: env.LIBRETRANSLATE_URL,
  // timeout: 1000,
  // withCredentials: true,
  headers: { "Content-Type": "application/json" },
});

type TranslationResponse = { translatedText: string };
export const translate = async (word: string): Promise<TranslationResponse> => {
  const body = JSON.stringify({
    q: word,
    source: "fi",
    target: "en",
    format: "text",
  });

  const response = await TRANSLATION_API.post<TranslationResponse>(
    "/translate",
    body,
  );

  return response.data;
};
