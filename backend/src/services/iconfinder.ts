import { env } from "../env/server";
import axios from "axios";
import {
  type IconFinderSearchResponse,
  type SimplifiedIconReponse,
} from "../types/iconfinder";

const SEARCH_COUNT = 50;

const API = axios.create({
  baseURL: "https://api.iconfinder.com/v4/",
  withCredentials: true,
  headers: {
    accept: "application/json",
    Authorization: `Bearer ${env.ICON_FINDER_API_KEY}`,
  },
});

export const searchIcon = async (
  searchterm: string,
): Promise<SimplifiedIconReponse> => {
  // const translation = await translate(searchterm);

  const response = await API.get<IconFinderSearchResponse>(
    `icons/search?query=${searchterm}&count=${SEARCH_COUNT}&vector=${true}&premium=${false}`,
  );
  // const responseFinnish = await API.get<IconFinderSearchResponse>(
  //   `icons/search?query=${translation.translatedText}&count=${SEARCH_COUNT}&vector=${true}&premium=${false}`,
  // );
  // const unique = [...new Set(data.map(item => item.group))]; // [ 'A', 'B']
  // ...responseFinnish.data.icons
  const allIcons = [...response.data.icons];

  const allUniqueIcons = [
    ...new Map(allIcons.map(icon => [icon.icon_id, icon])).values(),
  ];

  const icons = await Promise.all(
    allUniqueIcons.flatMap(async icon => {
      // if (!icon.vector_sizes[0].formats[0].download_url) return;
      const { data: svg } = await API.get<string>(
        icon.vector_sizes[0].formats[0].download_url,
      );

      return { svg, icon_id: icon.icon_id };
    }),
  );
  return { icons };
};
