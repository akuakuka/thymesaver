import { type User as PrismaUser } from "@prisma/client";

declare global {
  namespace Express {
    export interface Request {
      authenticatedUser: PrismaUser;
      //  user: User;
    }
  }
}

// to make the file a module and avoid the TypeScript error
export {};
