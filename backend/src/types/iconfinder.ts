type Format = {
  format: string;
  download_url: string;
};

export type IconFinderSearchResponse = {
  total_count: number;
  icons: Icon[];
};

type Icon = {
  icon_id: number;
  tags: string[];
  published_at: string;
  is_premium: boolean;
  type: string; // "vector"
  is_icon_glyph: boolean;
  containers: any;
  raster_sizes: any;
  vector_sizes: [
    {
      formats: Format[];
      target_sizes: number[][];
      size: number;
      size_width: number;
      size_height: number;
    },
  ];
};
export type SimplifiedIconReponse = {
  icons: Array<{ svg: string; icon_id: number }>;
};
