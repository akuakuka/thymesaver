import { type Item, type Prisma, type User, type List } from "@prisma/client";

type ListWithUsers = Prisma.ListGetPayload<{
  include: {
    users: true;
    actionlog: true;
    items: true;
  };
}>;

// type SocketActions = "add" | "remove" | "alert";
type CreateType<T> = Omit<T, "id" | "isDeleted" | "createdAt" | "updatedAt">;

interface ServerToClientEvents {
  add: () => (items: Item[]) => void;
  broadcastListStatus: (items: Item[]) => void;
  broadcastUsersLists: (user: ListWithUsers[]) => void;
}

interface ClientToServerEvents {
  addNewItem: (item: CreateType<Item>) => void;
  addItem: (item: Item) => void;
  removeItem: (item: Item) => void;
  editItem: (item: Item) => void;
  deleteItem: (item: Item) => void;
  addNewList: (list: CreateType<List>) => void;
  editList: (list: List) => void;
  deleteList: (list: List) => void;
  requestListItems: () => void;
}

interface InterServerEvents {
  ping: () => void;
}

interface SocketData {
  name: string;
  age: number;
}

// enum ListActionLogTypes {
//   LIST_CREATE = "LIST_CREATE",
//   LIST_UPDATE = "LIST_UPDATE",
//   LIST_DELETE = "LIST_DELETE",
// }

// enum ItemActionLogTypes {
//   ITEM_CREATE = "ITEM_CREATE",
//   ITEM_UPDATE = "ITEM_UPDATE",
//   ITEM_DELETE = "ITEM_DELETE",
//   ITEM_REMOVE = "ITEM_REMOVE",
//   ITEM_ADD = "ITEM_ADD",
// }

// type ActionLogActionType = ItemActionLogTypes | ItemActionLogTypes;

export type {
  User,
  ListWithUsers,
  Item,
  ServerToClientEvents,
  ClientToServerEvents,
  InterServerEvents,
  SocketData,
  CreateType,
};
