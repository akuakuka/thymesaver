import { Strategy as JwtStrategy, ExtractJwt } from "passport-jwt";
import { env } from "../env/server";
import { prisma } from "../db/db";

const secretOrKey = env.SESSION_KEY;

// JWT strategy
export const jwtStrategy = new JwtStrategy(
  {
    jwtFromRequest: ExtractJwt.fromHeader("x-auth-token"),
    secretOrKey,
  },
  async (payload, done) => {
    console.log("JWT STRATEGY");
    console.log(payload);
    try {
      const user = await prisma.user.findFirst({
        where: { id: payload.id },
      });

      // const user = await User.findById(payload.id);

      if (user) {
        done(null, user);
      } else {
        done(null, false);
      }
    } catch (err) {
      done(err, false);
    }
  },
);
