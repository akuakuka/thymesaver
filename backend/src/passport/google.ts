import { Strategy as GoogleStrategy } from "passport-google-oauth20";
import { env } from "../env/server";
import { prisma } from "../db/db";

export const googleStrategy = new GoogleStrategy(
  {
    clientID: env.GOOGLE_CLIENT_ID,
    clientSecret: env.GOOGLE_CLIENT_SECRET,
    callbackURL: env.GOOGLE_CALLBACK_URL,
    passReqToCallback: true,
  },
  async (accessToken, refreshToken, a, profile, done) => {
    const foundUser = await prisma.user.findFirst({
      where: { googleId: profile.id },
    });
    console.log("FOUND USER :: ", foundUser?.id);
    if (foundUser) {
      console.log("BEFORE DONE");
      done(null, foundUser);
      console.log("AFTER DONE");
      return;
    }
    const newUser = await prisma.user.create({
      data: {
        username: profile.displayName,
        googleId: profile.id,
        lists: {
          create: [{ name: "Default list" }],
        },
      },
    });
    done(null, newUser);
  },
);
