import { Router } from "express";
import passport from "passport";
import { env } from "../env/server";
import { generateJWT } from "../middleware/jwt";

export const authRouter = Router();

authRouter.get(
  "/google",
  // eslint-disable-next-line @typescript-eslint/no-unsafe-argument
  passport.authenticate("google", {
    scope: ["profile", "email"],
  }),
);

const clientUrl = env.FRONTEND_URL;

authRouter.get(
  "/google/callback",
  // eslint-disable-next-line @typescript-eslint/no-unsafe-argument
  passport.authenticate("google", {
    failureRedirect: "/",
    session: false,
  }),
  (req, res) => {
    console.log("CALLBACK");
    // eslint-disable-next-line @typescript-eslint/ban-ts-comment
    // @ts-expect-error
    const token = generateJWT(req.user);
    console.log({ token });
    //   res.cookie("x-auth-cookie", token, { sameSite: "lax" });
    res.redirect(`${clientUrl}?token=${token}`);
  },
);
