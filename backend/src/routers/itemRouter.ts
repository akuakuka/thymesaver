import { Router } from "express";
import { asyncMiddleware } from "../middleware/async";
import { prisma } from "../db/db";

export const itemRouter = Router();

itemRouter.put(
  "/",
  // eslint-disable-next-line @typescript-eslint/no-misused-promises
  asyncMiddleware(async (req, res) => {
    const item = await prisma.item.update({
      where: { id: req.body.id },
      data: { ...req.body },
    });

    res.json(item);
  }),
);

itemRouter.get(
  "/",
  // eslint-disable-next-line @typescript-eslint/no-misused-promises
  asyncMiddleware(async (req, res) => {
    const itemId = req.query.searchTerm;
    if (typeof itemId !== "string") {
      return res.status(400).send("itemId required");
    }
    const item = await prisma.item.findFirstOrThrow({
      where: { id: itemId },
      include: { actionlog: true },
    });
    res.json(item);
  }),
);

itemRouter.delete(
  "/:id",
  // eslint-disable-next-line @typescript-eslint/no-misused-promises
  asyncMiddleware(async (req, res) => {
    const itemId = req.params.id;
    if (typeof itemId !== "string") {
      return res.status(400).send("itemId required");
    }
    const item = await prisma.item.delete({
      where: { id: itemId },
    });
    res.json(item);
  }),
);
