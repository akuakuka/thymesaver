/* eslint-disable @typescript-eslint/no-misused-promises */
import { Router } from "express";
import { prisma } from "../db/db";
import { asyncMiddleware } from "../middleware/async";

export const userRouter = Router();

userRouter.get(
  "/search",
  asyncMiddleware(async (req, res) => {
    const searchTerm = req.query.term;
    if (typeof searchTerm !== "string") {
      return res.status(400).send("search term required");
    }
    const found = await prisma.user.findMany({
      where: { username: { contains: searchTerm } },
    });
    res.json(found);
  }),
);

userRouter.get("/me", async (req, res) => {
  try {
    const user = await prisma.user.findFirstOrThrow({
      // eslint-disable-next-line @typescript-eslint/ban-ts-comment
      // @ts-expect-error
      where: { id: req?.user?.id },
      include: { lists: { include: { users: true, items: true } } },
    });
    console.log(user);
    res.json(user);
  } catch (e) {
    console.log(e);
    res.json(e);
  }
});
