import { Router } from "express";
import { asyncMiddleware } from "../middleware/async";
import { searchIcon } from "../services/iconfinder";
export const iconRouter = Router();

iconRouter.get(
  "/search",
  // eslint-disable-next-line @typescript-eslint/no-misused-promises
  asyncMiddleware(async (req, res) => {
    const searchTerm = req.query.searchTerm;
    if (typeof searchTerm !== "string") {
      return res.status(400).send("search term required");
    }
    const result = await searchIcon(searchTerm);
    res.json(result);
  }),
);
