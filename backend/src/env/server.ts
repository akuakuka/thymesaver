import { createEnv } from "@t3-oss/env-core";
import { z } from "zod";

import dotenv from "dotenv";
dotenv.config();
console.log("####");
console.log("PORT");
console.log(process.env.PORT);

console.log("DATABASE_URL");
console.log(process.env.DATABASE_URL);

export const env = createEnv({
  server: {
    PORT: z
      .string()
      .transform(s => parseInt(s, 10))
      .pipe(z.number()),
    SESSION_KEY: z.string().min(10),
    GOOGLE_CLIENT_ID: z.string().min(10),
    GOOGLE_CLIENT_SECRET: z.string().min(10),
    GOOGLE_CALLBACK_URL: z.string().url(),
    FRONTEND_URL: z.string().url(),
    ICON_FINDER_CLIENT_ID: z.string().min(10),
    ICON_FINDER_API_KEY: z.string().min(10),
    LIBRETRANSLATE_URL: z.string().url(),
    DATABASE_URL: z.string(),
  },
  runtimeEnv: process.env,
  emptyStringAsUndefined: true,
});

console.log(env.PORT);
